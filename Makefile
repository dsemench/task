NAME = task

FLAGS = -Wall -Wextra -Werror

SRC = main.cpp

OB = $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OB)
			@clang++ -o $(NAME) $(OB)
			@echo "task created"

%.o:%.cpp
			@clang++ $(FLAGS) -c -o $@ $<

clean:
			@rm -rf $(OB)
			@echo "files *.o deleted"

fclean: clean
			@rm -rf $(NAME)
			@echo "all deleted"

re: clean all
