#include <iostream>
#include <vector>
#include <map>
#include <cstdlib>


using namespace std;

void	print_conteiners(const vector<int> vec, const map<int, int> mp) {
	cout << "vector [" << vec.size() << "] : ";
	for (size_t j = 0; j < vec.size(); j++) {
		cout << j << " = " << vec[j] << "; ";
	}
	cout << "\n";

	cout << "map    [" << mp.size() << "] : ";
		for (map<int, int>::const_iterator it = mp.begin(); it != mp.end(); it++) {
		cout << it->first << " = " << it->second << "; ";
	}
	cout << "\n";
}

int		main() {
	int				i = 0, rand_nbr;
	size_t			j;
	const char		*in_nbr;
	string			in;
	vector<int>		v_nbr;
	map<int, int>	m_nbr;
			/** validation **/
	while (i < 16 || i > 20) {
		cout << "Please input number for 16 till 20 to create containers\n";
		cin >> in;
		in_nbr = in.c_str();
		for (j = 0; isdigit(in_nbr[j]); j++);
		if (in_nbr[j] != '\0') {
			cout << "\"" << in_nbr[j] << "\" It isn't number! Try again\n";
		}
		else {
			i = atoi(in_nbr);
			if (i < 16 || i > 20)
				cout << "i = " << i << " Wrong number! Try again\n";
		}
	}
			/** add random elements in vector and map **/
	cout << "Your number is " << i << "\n";
	srand (time(0));
	for (j = 0; j < (size_t)i; j++) {
		rand_nbr = (rand() % 9) + 1;
		v_nbr.push_back(rand_nbr);
		rand_nbr = (rand() % 9) + 1;
		m_nbr.insert(pair<int, int >(j, rand_nbr));
	}
	print_conteiners(v_nbr, m_nbr);
	rand_nbr = rand() % 15;
	v_nbr.erase(v_nbr.begin(), v_nbr.begin() + rand_nbr);
	m_nbr.erase(m_nbr.begin(), m_nbr.find(rand_nbr));
	cout << "Print after deleted elements\n";
	print_conteiners(v_nbr, m_nbr);
			/** synchronization **/
	for (map<int, int>::iterator it = m_nbr.begin(); it != m_nbr.end(); ) {
		for (j = 0; j < v_nbr.size(); j++) {
			if (it->second == v_nbr[j])
				break ;
		}
		if (j == v_nbr.size())
			m_nbr.erase(it++);
		else
			it++;
	}
	for (j = 0; j < v_nbr.size(); j++) {
		map<int, int>::iterator it;
		for (it = m_nbr.begin(); it != m_nbr.end(); it++) {
			if (it->second == v_nbr[j])
				break ;
		}
		if (it == m_nbr.end()) {
			v_nbr.erase(v_nbr.begin() + j);
			j = 0;
		}
	}
	cout << "Result!\n";
	print_conteiners(v_nbr, m_nbr);
	return 0;
}