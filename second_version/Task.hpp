//
// Created by Dmitro Semenchuk on 9/5/17.
//

#ifndef TASK_TASK_HPP
#define TASK_TASK_HPP

#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Task {
private:
	vector<int>		_v_nbr;
	map<int, int>	_m_nbr;
public:
	Task(int size);
	~Task() {};

	void print_all();
	void del_elements(int size);
	void synchron();
};


#endif //TASK_TASK_HPP
