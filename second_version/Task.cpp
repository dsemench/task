//
// Created by Dmitro Semenchuk on 9/5/17.
//

#include "Task.hpp"

Task::Task(int size) {
	for (int i = 0; i < size; i++) {
		_v_nbr.push_back((rand() % 9) + 1);
		_m_nbr.insert(pair<int, int>(i, (rand() % 9) + 1));
	}
}

void Task::print_all() {
	cout << "map    [" << _m_nbr.size() << "] : ";
	for (map<int, int>::iterator i = _m_nbr.begin(); i != _m_nbr.end(); i++) {
		cout << i->first << " = " << i->second << "; ";
	}
	cout << "\n";
	cout << "vector [" << _v_nbr.size() << "] : ";
	for (size_t i =0; i < _v_nbr.size(); i++) {
		cout << i << " = " << _v_nbr[i] << "; ";
	}
	cout << "\n";
}

void Task::del_elements(int size) {
	cout << "size = " << size << "\n";
	_v_nbr.erase(_v_nbr.begin(), _v_nbr.begin() + size);
	_m_nbr.erase(_m_nbr.begin(), _m_nbr.find(size));
//	print_all();
	cout << "exit = " << size << "\n";

}

void Task::synchron() {
	size_t i;
	for (map<int, int>::iterator it = _m_nbr.begin(); it != _m_nbr.end(); ) {
		for (i = 0; i < _v_nbr.size(); i++) {
			if (_v_nbr[i] == it->second)
				break ;
		}
		if (i == _v_nbr.size())
			_m_nbr.erase(it++);
		else
			it++;
	}
	for (i = 0; i < _v_nbr.size(); i++) {
		map<int, int>::iterator it;
		for (it = _m_nbr.begin(); it != _m_nbr.end(); it++) {
			if (it->second == _v_nbr[i])
				break ;
		}
		if (it == _m_nbr.end()) {
			_v_nbr.erase(_v_nbr.begin() + i);
			i = 0;
		}
	}
}
