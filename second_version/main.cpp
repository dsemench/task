#include "Task.hpp"

int		main() {
	int				i = 0;
	size_t			j;
	const char		*in_nbr;
	string			in;

		/** validation **/

	while (i < 16 || i > 20) {
		cout << "Please input number for 16 till 20 to create containers\n";
		cin >> in;
		in_nbr = in.c_str();
		for (j = 0; isdigit(in_nbr[j]); j++);
		if (in_nbr[j] != '\0') {
			cout << "\"" << in_nbr[j] << "\" It isn't number! Try again\n";
		}
		else {
			i = atoi(in_nbr);
			if (i < 16 || i > 20)
				cout << "i = " << i << " Wrong number! Try again\n";
		}
	}

		/** add random elements in vector and map **/

	cout << "Your number is " << i << "\n";
	srand (time(0));

	Task t = Task(i);
	t.print_all();
	cout << "Print after deleted elements\n";
	t.del_elements(rand() % 15 + 1);
	t.print_all();

		/** synchronization **/

	t.synchron();
	cout << "Result!\n";
	t.print_all();
	return 0;
}